
public class Percolation {
    private int rows;
    private int columns;
    private int dimen;
    private boolean[] siteOpen;
    private int adjacentSeed;
    private int seed;
    private int N;
    
    private WeightedQuickUnionUF wQU;
    
    public Percolation(int N) // create N-by-N grid, with all sites blocked
    {
        this.N = N;
        setRows(N);
        setColumns(N);
        setDimen(getRows() * getColumns());
        siteOpen = new boolean[dimen];
        setAdjacentSeed(0);
        setSeed(0);
        
        wQU = new WeightedQuickUnionUF(dimen+2);
        
    }
    
    private int getRows() {
        return rows;
    }
    
    private int getColumns() {
        return columns;
    }
    
    private int getDimen() {
        return dimen;
    }
    
    private int getAdjacentSeed() {
        return adjacentSeed;
    }
    
    private int getSeed() {
        return seed;
    }
    
    private void setRows(int i) {
        rows = i;
    }
    
    private void setColumns(int i) {
        columns = i;
    }
    
    private void setDimen(int i) {
        dimen = i;
    }
    
    private void setAdjacentSeed(int i) {
        adjacentSeed = i;
    }
    
    private void setSeed(int i) {
        seed = i;
    }
    
    private void validationIndeces(int i, int j) {
        if (i <= 0 || i > N) throw 
            new IndexOutOfBoundsException("row index i out of bounds");
        if (j <= 0 || j > N) throw 
            new IndexOutOfBoundsException("column index i out of bounds");
        
    }
    public void open(int i, int j) {  // open site (row i, column j) if not already 
        validationIndeces(i, j);
        setSeed((getRows() * (i-1)) + (j-1));
        siteOpen[getSeed()] = true;
        
        if ((i > 1) && isOpen(i-1, j)) 
        {
            setAdjacentSeed((getRows() * (i-2)) + (j-1));
            wQU.union(getSeed(), getAdjacentSeed());
        }
        
        //Lower Cell
        if ((i < getRows()) && isOpen(i+1, j)) 
        {
            setAdjacentSeed(((getRows() * i) + (j-1)));
            wQU.union(getSeed(), getAdjacentSeed());
        }
        
        //Left Cell
        if ((j > 1) && isOpen(i, j-1))
        {
            setAdjacentSeed(((getRows() * (i - 1)) + (j - 2)));
            wQU.union(getSeed(), getAdjacentSeed());
        }
        
        //Right Cell
        if ((j < getColumns()) && isOpen(i, j+1)) 
        {
            setAdjacentSeed(((getRows() * (i - 1)) + (j)));
            wQU.union(getSeed(), getAdjacentSeed());
        }       
        
        if (i == 1) {
            wQU.union(getDimen(), getSeed());
        }
        
        if (i == getRows()) {
            wQU.union(getDimen()+1, getSeed());
        }
    }
    
    public boolean isOpen(int i, int j) { // is site (row i, column j) open?
        validationIndeces(i, j);
        return siteOpen[((getRows() * (i-1)) + (j-1))];
    }
    
    public boolean isFull(int i, int j) { // is site (row i, column j) full?
        validationIndeces(i, j);
        if (isOpen(i, j)) {
            setSeed((getRows() * (i-1)) + (j-1));
            if (wQU.connected(getDimen(), getSeed()))
                return true;
        }
        return false;
    }
    
    public boolean percolates() {     // does the system percolate?
        if (wQU.connected(getDimen(), getDimen()+1))
            return true;
        return false;
    }
}