import java.util.*;
import java.io.*;
public class Fast {
    
    public static void main (String []args) {
        int numberOfPoints = 0;
        Point[] pointArr = null;
        Point[] secondPointArr = null;
        
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.setPenRadius(0.005);
        
        try {
            Scanner sc = new Scanner (new File (args[0]));
            numberOfPoints = sc.nextInt();
            pointArr = new Point [numberOfPoints];
            secondPointArr = new Point [numberOfPoints];
            int it = 0;
            while (sc.hasNext()) {
                pointArr[it++] = new Point(sc.nextInt(), sc.nextInt());
            }
        }
        
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
        
        Arrays.sort (pointArr);
        
        int numOfSlopes = 0;
        int i=0,j=0,k=0,l=0,m=0;
        double matchSlp = 0;
        
        outer:for (i = 0; i < numberOfPoints; i++) 
        {                
            secondPointArr = Arrays.copyOf (pointArr, numberOfPoints);;
            Arrays.sort (secondPointArr, i, numberOfPoints, secondPointArr[i].SLOPE_ORDER);
            
            for (j = i+1; j < numberOfPoints; ++j) {
                
                for (numOfSlopes =0, k = j, matchSlp = secondPointArr[i].slopeTo(secondPointArr[k]); 
                     (k < numberOfPoints) && (matchSlp == secondPointArr[i].slopeTo(secondPointArr[k])) ;
                     k++ )
                {
                    ++numOfSlopes;    
                }
                if (numOfSlopes > 2) {
                    
                    for ( l = 0; l < i; l++ ){
                        if (matchSlp == secondPointArr[i].slopeTo(secondPointArr[l])) break outer;
                    }
                    
                    System.out.print (secondPointArr[i]) ;
                    for (m= j; m < k; m++) {
                        System.out.print ( " -> " + secondPointArr[m]);
                    }
                    secondPointArr[i].drawTo(secondPointArr[m-1]);
                    System.out.println();
                    if (numOfSlopes == 4) break outer;

                }  
            }
        }
        StdDraw.show(0);
    }
}
