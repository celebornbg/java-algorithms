
import java.util.Scanner;
import java.util.Arrays;
import java.io.File;
import java.io.FileNotFoundException;

public class Brute {
    
    public static void main (String []args) {
        int pointObjects = 0;
        Point[] point = null;
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.setPenRadius(0.005);
        StdDraw.show(0);
        try {
            if (args.length > 0) {
                String filename = args[0];
                Scanner sc = new Scanner (new File (filename));
                pointObjects = sc.nextInt();
                point= new Point [pointObjects];
                int it = 0;
                while (sc.hasNext()) {
                    point[it++] = new Point(sc.nextInt(),sc.nextInt());
                    point[it-1].draw();
                }
            }
        }
        catch (FileNotFoundException e ) {
            e.printStackTrace();
        }
        
        double slope = 0;
        Point[] pointOnLine = new Point[4];
        
        
        
        for (int i = 0 ; i < pointObjects; ++i) {
            for (int j = 0; j < pointObjects; ++j) {
                if (i == j) continue;
                slope = point[i].slopeTo(point[j]);
                for (int k = 0; k < pointObjects; ++k) {
                    if (i==k || j == k) continue;
                    if (slope !=  point[j].slopeTo(point[k])) continue;
                    for (int l = 0 ; l < pointObjects; ++l) {
                        if (i == l || j==l ||  k==l) continue;
                        if (slope != point[k].slopeTo(point[l])) continue;
                        pointOnLine[0] = point[i];
                        pointOnLine[1] = point[j];
                        pointOnLine[2] = point[k];
                        pointOnLine[3] = point[l];
                        if ((pointOnLine[0].compareTo(pointOnLine[1]) == -1) && 
                            (pointOnLine[1].compareTo(pointOnLine[2]) == -1) &&
                            (pointOnLine[2].compareTo(pointOnLine[3]) == -1)) 
                        { System.out.println (pointOnLine[0] + " -> " + pointOnLine[1] + " -> " + pointOnLine[2] + " -> " + pointOnLine[3]);
                            pointOnLine[0].drawTo(pointOnLine[3]);}
                    }                     
                }
                
            }   
        }
        StdDraw.show(0);
    }
}
