package org.random_contraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class RandomContraction {

	/**
	 * @param args
	 * @throws Exception 
	 * 
	 *
	 */
	static Integer randomV1 = 0;
	static Integer randomV1Index = 0;
	static Integer randomV1Size = 0;
	static Integer randomV2IndV1 = 0;
	static Integer randomV2 = 0;
	static Integer randomV2Index =0;
	static Integer randomV2Size = 0;
	static int totalSize = 0;
	static int totalInd = 0;

	static ArrayList<ArrayList <Integer>> aList =  new ArrayList<ArrayList<Integer>>();
	static ArrayList<Integer> tmpList ;
	
	static String input;
	
	public static void randomGen() {
		Random randomNum = new Random ();

		totalSize = aList.size();
		totalInd = totalSize -1 ;
		//System.out.println ("Size : "+ totalSize);
		randomV1Index = randomNum.nextInt(totalInd);
		randomV1 = randomV1Index + 1;

		randomV1Size = aList.get(randomV1Index).size() - 1;

		/*System.out.println ("Vertex 1 Index: " + randomV1Index);
		System.out.println ("Vertex 1 : " + randomV1);
		System.out.println ("Vertex 1 Size : " + randomV1Size);
*/
		randomV2IndV1 = randomNum.nextInt(randomV1Size.intValue());
		//System.out.println ("Vertex 2 Index : " + randomV2IndV1);

		randomV2 = aList.get(randomV1Index).get(randomV2IndV1);		
		randomV2Index = randomV2 -1;
		//System.out.println ("Vertex 2 : " + randomV2);
	}

	public static void includeV2InV1() {

		// Checking function to check if the variables are filled in the tempV2 properly
		for (Integer item: tmpList) {
			if (item != randomV1){ 
				aList.get(randomV1Index).add(item);
			}
		}
	}

	public static void removeV2InV1() {
		int index = 0;
		// remove v2 which is in v1
		if  (aList.get(randomV1Index).contains(randomV2)) {
			index = aList.get(randomV1Index).indexOf(randomV2);
			//System.out.println ("Index to be removed : " + index);
			//System.out.println ("Value of the Index : " + aList.get(randomV1Index).get(index));
			aList.get(randomV1Index).remove(index);
		}
	}

	public static void replaceV2WithV1() {
		int tmpInd =0;
		while (tmpInd <= totalInd) {
			int index = 0;
			if (aList.get(tmpInd).contains(randomV2))
			{	
				index = aList.get(tmpInd).indexOf(randomV2);
				aList.get(tmpInd).set(index,randomV1);
			}	
			++tmpInd;
		}	
	}
	public static void adjustVerticeValues() {
		totalInd = aList.size() -1;
		int tmp = 0;
		while (tmp <= totalInd) {
			int tmp2 = 0;
			int tmpVSize = aList.get(tmp).size();
			while (tmp2 < tmpVSize) {
				if (aList.get(tmp).get(tmp2) > randomV2)
					aList.get(tmp).set(tmp2,aList.get(tmp).get(tmp2) - 1);
				++ tmp2;
			}
			++tmp;
		}

	}
	public static void main(String[] args) throws Exception {

		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		System.out.print ("Enter the file with suitable data (Hint: See the text file) : ");

		input = keyboard.readLine();
		
		Scanner sc =new Scanner(new File(input));

		String line;
		sc.useDelimiter("\\n");
		int vertexIndex = 0;
		int tmp = 0;
		while (sc.hasNextLine()) {
			int edge = 0;
			line = sc.nextLine();
			Scanner lineSc = new Scanner(line);
			lineSc.useDelimiter("\\s");
			vertexIndex = lineSc.nextInt() - 1;
			aList.add(new ArrayList<Integer>());
			tmp = 0;
			while (lineSc.hasNextInt()) {
				edge = lineSc.nextInt();
				aList.get(vertexIndex).add(edge);
				++tmp;
			}
		}
		while (aList.size() > 2) {

			randomGen();
			//System.out.println ("random v2 index in main : " + randomV2Index); 
			tmpList = new ArrayList <Integer>() ;
			
			removeV2InV1();

			includeV2InV1();
			totalInd = aList.size()-1;	
			replaceV2WithV1();
			aList.remove(randomV2Index.intValue());
			adjustVerticeValues();
			tmpList.clear();
		}
		System.out.println(aList.get(1).size());
		System.out.println(aList.get(0).size());
		aList.clear();
	}

}
