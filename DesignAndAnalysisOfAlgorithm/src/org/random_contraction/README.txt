The testcase text file contains an adjacency list representation of a simple undirected graph.
The program runs karger randomized contraction algorithm to calculate the min cut.
Note: The program has to be run several time to see the best possible answer. Every now and then the program throws and index exception which has to be investigated.