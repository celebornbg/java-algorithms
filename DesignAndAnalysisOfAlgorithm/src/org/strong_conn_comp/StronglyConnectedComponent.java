package org.strong_conn_comp;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class StronglyConnectedComponent {

	/**
	 * @param args
	 */
	static int N ;

	@SuppressWarnings("unchecked")
	static ArrayList<Integer>[] gRev =  (ArrayList<Integer>[]) new ArrayList[N];
	@SuppressWarnings("unchecked")
	static ArrayList<Integer>[] gOrg =   (ArrayList<Integer>[])new ArrayList[N];

	static ArrayList<Integer> scc = new ArrayList<Integer>();
	
	static int count = 1;
	
	static boolean[] visited = new boolean[N];
	
	static int[] f = new int[N];
	static int[] g = new int[N];

	static int[] leader = new int[N]; 
	
	static int t = 0; // finishing time in 1st pass (# of nodes processed so far)
	
	static int s = 0; // current source vertex (leaders in 2nd pass)
	
	static String input;
	
	public static void DFS_Loop(ArrayList<Integer>[] G, int choice) {
		for (int i = (N-1); i >= 1; i--) {
			if (visited[i] == false && choice == 0) {
					DFSR(G, i);
				}
			if (visited[g[i]] == false && choice == 1) {
					s = g[i];
					DFSO(G, g[i]);
					scc.add(count);
					count = 1;
				}
			}
		}
	
	
	public static void DFSR(ArrayList<Integer>[] G, int node) {
		visited[node] = true;
		int jSize = G[node].size();
		int a = 0;
		while (jSize > a) {
			int index = G[node].get(a);
			if (visited[index] == false) {
				DFSR (G, index);
			}
			++a;			
		}
		++t;
		g[t] = node;
	}
	public static void DFSO(ArrayList<Integer>[] G, int node) {
		visited[node] = true;
		int jSize = G[node].size();
		leader[node] = s;
		for (int a = jSize-1; a >= 0; --a) {
			int index = G[node].get(a);
			if (visited[index] == false) {
				DFSO (G,index);
				++count;
			}
		}
	}
	
	
	public static void main(String[] args) throws IOException  {
		// SimpleSmallOne : 3,1,1, N = 6
		// TestCase1 : [3,3,3,3,1] , N = 14
		// TestCase2 : 7-6-5-5-4 , N = 31
		// TestCase3 : 152, 88, 82, 76, 66, 46, 38, 33, 30, 29, 28, 21 ... , N = 1001
		
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		System.out.print ("Enter the number of vertices : ");
		N = keyboard.read();
		
		keyboard = new BufferedReader(new InputStreamReader(System.in));
		System.out.print ("Enter the file location with data in suitable format and correct representation (Hint: See the text file) : ");
		input = keyboard.readLine();
		
		Scanner sc = new Scanner(new File(input));
		Scanner lineSc;
		String line;
		sc.useDelimiter("\\n");
		long start = System.currentTimeMillis(); 
	
		for (int i = 1; i < N; ++i) {
			gOrg[i] = new ArrayList<Integer>();
			gRev[i] = new ArrayList<Integer>();
			visited[i] =false;
		}
		
		while (sc.hasNextLine()) {
			line = sc.nextLine();
			lineSc = new Scanner(line);
			
			Integer tail = lineSc.nextInt();
			Integer head = lineSc.nextInt();
			
			int tailInt = tail.intValue();
			int headInt = head.intValue();
			
			//Original Graph
			gOrg[tailInt].add(headInt);
			//Reverse Graph
			gRev[headInt].add(tailInt);
			lineSc.close();
		}
		sc.close();
		long end = System.currentTimeMillis(); 
		
		DFS_Loop(gRev,0);
		for (int i = 1; i < N; ++i) {
			visited[i] =false;
		}
		DFS_Loop(gOrg,1);
		
		System.out.println("Execution speed "+ ((end - start)/60000)+" min\n"); 
		
		Comparator comparator = Collections.reverseOrder();
		Collections.sort(scc, comparator);
		
		for (int i = 0; i < 5; ++i) {
			System.out.println(scc.get(i));	
		}
	}
}
