package org.merge_sort;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class CountingInversion {

	static String input;
	
	public static long divideW (int Arr[]) {
		long countLeft ;
		long countRight ;
		long countMerge ;
		
		int mid = (Arr.length)/2;
		
		if (Arr.length <= 1)
			return 0;
		else
		{
			int leftArr[] = new int [mid];
			int rightArr[] = new int [Arr.length - mid];
			
			for (int i = 0; i < mid; i++){
				leftArr[i] = Arr[i];
			}
			for (int j = 0; j < rightArr.length ; j++){
				rightArr[j] = Arr[mid + j];
			}	
			countLeft = divideW (leftArr);
			countRight = divideW (rightArr);
			
			int[] result = new int[Arr.length];
			countMerge = conquer(leftArr, rightArr, result);
			
			for (int k=0; k<Arr.length; k++){
	            Arr[k]=result[k];
	        }

			return (countLeft + countRight + countMerge);
		}
	}
	public static long conquer (int []l, int[]r, int[] result) {
		int i = 0;
		int j = 0;
		int k = 0;
		long count = 0;
		while ((i < l.length) && (j < r.length)) {
		if (l[i] <= r [j]) {
			result[k] = l[i++];
		}
		else if (l[i] > r[j]) {
			result[k] = r[j++];
			count += l.length - i;
		}
		++k;
		}
		while ( i < l.length) {
			result[k++] = l[i++];
		}
		while ( j < r.length) {
			result[k++] = r[j++];
		}
		return count;
	}
	
	
	public static void main(String[] args) {
		CountingInversion rs = new CountingInversion();
		long count=0;
        Integer n[];
        int i=0;
        try{
            n=OpenFile();
            int num[] = new int[n.length];
            for (i=0;i<n.length;i++){
                num[i]=n[i].intValue();
            }
            count=rs.divideW(num);

        }
        catch(Exception e){
            e.printStackTrace();
        }
        System.out.println("The number of inversions " + count);
	}
	public static Integer[] OpenFile() throws IOException{

	    BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		System.out.print ("Enter the file with suitable data (Hint: See the text file) : ");

		input = keyboard.readLine();
		
		FileReader fr=new FileReader(input);

	    BufferedReader textR= new BufferedReader(fr);
	    int nLines=readLines();
	    System.out.println("Number of lines "+nLines);

	    Integer[] nData=new Integer[nLines];
	    for (int i=0; i < nLines; i++) {
	        nData[ i ] = Integer.parseInt((textR.readLine()));

	    }
	    textR.close();

	    return nData;

	}

	public static int readLines() throws IOException{

	    FileReader fr=new FileReader(input);
	    BufferedReader br=new BufferedReader(fr);

	    int numLines=0;

	    while(br.readLine()!=null){
	        numLines++;
	    }
	    System.out.println("Number of lines readLines "+ numLines);
	    return numLines;

	}


}