// Number of comparision during a QuickSort when median is choosed as a pivot (3- way median rule).
package org.quick_sort;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class QSortNumOfComparision {

int pivotPosition = 0;
static int []result;
static int comparision = 0;
static String input;

public static void QuickSort (int []Arr, int left, int right) {
	int p= 0;
	if (left == right)
		return;
	p = partition(Arr, left, right);
	result = new int [Arr.length];
	QuickSort (Arr, left, p );
	comparision = (p -left) + comparision;
	QuickSort (Arr, p+1, right);
	comparision = (right -(p+1)) + comparision;

	
	for (int k=0; k<Arr.length; k++){
        result[k]=Arr[k];
    }
}
	
	public static int partition (int []Arr, int left, int right) {
		
		int pElem = 0;
		if ((right + left )% 2 == 1) {
			int a = Arr[left];
			int b = Arr[(((left + right +1 )/2)-1)];
			int c = Arr[right-1];

			int median = 0;
			
		    if ((b > a && b < c) || (b < a && b > c)) { 
		    	median = b;
		    	pElem = median;
				Arr[(((left + right +1 )/2)-1)] = Arr[left];
				Arr[left] = pElem;
		    }
		     if ((a >= b && a <= c) || (a <= b && a >= c)) {
		    	median= a;
		    	pElem = median;
		    }
		     if ((c >= a && c <= b) || (c <= a && c >= b)) { 
		    	median= c;
		    	pElem = median;
				Arr[right-1] = Arr[left];
				Arr[left] = pElem;
		    }
		}
		if ((right + left) % 2 == 0){
			int a = Arr[left];
			int b = Arr[(((left + right )/2)-1)];
			int c = Arr[right-1];

			int median = 0;
		    
			if ((b > a && b < c) || (b < a && b > c)) { 
		    	median = b;
		    	pElem = median;
				Arr[(((left + right )/2)-1)] = Arr[left];
				Arr[left] = pElem;
		    }
			
		    if ((a >= b && a <= c) || (a <= b && a >= c)) {
		    	median= a;
		    	pElem = median;		   
		    }
		     if ((c >= a && c <= b) || (c <= a && c >= b)) { 
		    	median= c;
		    	pElem = median;
				Arr[right-1] = Arr[left];
				Arr[left] = pElem;
			   		    }
			
		}
		
		int i = left + 1;
		for (int j = left+1; j < right; ++j)
		{
			if (Arr[j] < pElem ) {
				int tj = Arr[j];
				Arr[j] = Arr[i];
				Arr[i] = tj;
				++i;
			}
		}
		Arr[left] = Arr[i-1];
		Arr[i-1] = pElem;
		return (i-1);
	}
	
	public static void main(String[] args) {
		// Answers
		/*{0,9,8,7,6,5,4,3,2,1}
		L    = 45
		R    = 37
		Med  = 25

		Data: {0,1,2,3,4,5,6,7,8,9}
		L   = 45
		R   = 45
		Med = 19

		First 5000 (see link to 1/2 of my 10,000 file)
		L   = 71934
		R   = 74338
		Med = 59686
		
		*
		ARRAY: 1, 11, 5, 15, 2, 12, 9, 99, 77, 0   # of comps =22
		ARRAY: 999,3,2,98,765,8,14,15,16,88,145,100   # of comps =29
		ARRAY: 1,11,5,15,2,999,3,2,98,765,8,14,15,16,88,145,100,12,9,99,77,0    # of comps = 82
		*
		*/
		
		long count=0;
        Integer n[];
        int i=0;
        try{
            n=OpenFile();
            int num[] = new int[n.length];
            for (i=0;i<n.length;i++){
                num[i]=n[i].intValue();
           }
            int len = num.length;
            QuickSort (num, 0, len);

        }
        catch(IOException e){
            e.printStackTrace();
        }
        System.out.println("The number of comparision " + comparision);

	}
	public static Integer[] OpenFile() throws IOException{

		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		System.out.print ("Enter the file with suitable data (Hint: See the text file) : ");

		input = keyboard.readLine();
	    FileReader fr=new FileReader(input);// to put in file name.

	    BufferedReader textR= new BufferedReader(fr);
	    int nLines=readLines();
	    System.out.println("Number of lines "+nLines);

	    Integer[] nData=new Integer[nLines];
	    for (int i=0; i < nLines; i++) {
	        nData[ i ] = Integer.parseInt((textR.readLine()));

	    }
	    textR.close();

	    return nData;

	}

	public static int readLines() throws IOException{

	    FileReader fr=new FileReader(input);
	    BufferedReader br=new BufferedReader(fr);

	    int numLines=0;

	    while(br.readLine()!=null){
	        numLines++;
	    }
	    System.out.println("Number of lines readLines "+ numLines);
	    return numLines;

	}
}
