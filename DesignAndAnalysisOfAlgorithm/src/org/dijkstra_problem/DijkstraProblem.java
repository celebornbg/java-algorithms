package org.dijkstra_problem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class Vertex implements Comparable<Vertex> {

	public final Integer id;
	public ArrayList<Edge> adjacencies;
	public Integer minDistance = Integer.MAX_VALUE;
	public Vertex previous;
	
	public Vertex (Integer argId) {id = argId;}
	
	public int compareTo(Vertex other) {
		return Double.compare(minDistance, other.minDistance);
	}
	
}

class Edge {
	public final Vertex target;
	public final Integer weight;
	public Edge(Vertex argTarget, Integer argWeight) {
		target = argTarget;
		weight = argWeight;
	}
	
}
public class DijkstraProblem {

	static Integer N;
	
	static String input;
	
	public static void computePath (Vertex source) {
		source.minDistance = 0;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
      	vertexQueue.add(source);

	while (!vertexQueue.isEmpty()) {
	    Vertex u = vertexQueue.poll();

            // Visit each edge exiting u
            for (Edge e : u.adjacencies)
            {
                Vertex v = e.target;
                Integer weight = e.weight;
                Integer distanceThroughU = u.minDistance + weight;
                if (distanceThroughU < v.minDistance) {
                	vertexQueue.remove(v);
                	v.minDistance = distanceThroughU;
                	v.previous = u;
                	vertexQueue.add(v);
                }
            }
		}
	}
	
	public static List<Vertex> getShortestPathTo (Vertex target) {
        List<Vertex> path = new ArrayList<Vertex>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
            path.add(vertex);
        Collections.reverse(path);
        return path;
	}
	
	public static void main(String[] args) throws IOException {
	
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		System.out.print ("Enter the number of vertices : ");
		N = keyboard.read();
		
		keyboard = new BufferedReader(new InputStreamReader(System.in));
		System.out.print ("Enter the file location with data in suitable format and correct representation (Hint: See the text file) : ");
		input = keyboard.readLine();
		
		Scanner sc = new Scanner(new File(input));
		Vertex V[] = new Vertex[N];
		Scanner lineSc;
		String lineStr;
		
		Integer edge = 0, length = 0;
		String strE, strL ;
		
		String lineRegEx = "(\\d+),(\\d+)";
		Pattern linePattern = Pattern.compile(lineRegEx);
		Matcher matcher;
		sc.useDelimiter("\\n");
		
		for (int i = 0; i < N; ++i) {
			V[i] = new Vertex(i+1);
		}
		int outCount = 0;
		while (sc.hasNextLine()) {
			
			lineStr = sc.nextLine();
			lineSc = new Scanner(lineStr);
			
			lineSc.nextInt(); 
			
			matcher = linePattern.matcher(lineStr);
			
			ArrayList <Edge> edges = new ArrayList<Edge>();
			while (matcher.find()) {
				strE = matcher.group(1);
				edge = Integer.parseInt(strE);
				
				strL = matcher.group(2);
				length = Integer.parseInt(strL);

				edges.add(new Edge(V[edge-1], length));
					
			}
			V[outCount].adjacencies = edges;
			++outCount;
			lineSc.close();
		}
		sc.close();
		computePath (V[0]);
				
		//SimpleTestCase1 : Answer:[1,2,3,4,5,6,7,8]=[0,3,2,4,7,9,7,12]				
		//SimpleTestCase2 : 1 ---> 0, 2 ---> 7, 3 ---> 5, 4 ---> 7, 5 ---> 6

		 for (Vertex v : V)
			{
			    System.out.println("Distance to " + v.id + ": " + v.minDistance);
			}
	}

}
